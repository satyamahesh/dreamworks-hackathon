#include "MapLoader.h"
#include "Vector.h"
#include <cstring>
#include <openvdb/openvdb.h>
#include <stdlib.h>
#include <fstream>
using namespace std;

/************************ This loads the map from openVDB.
                          You can use the native viewer once you complete this.
                          There are comments for each snippet of code.
                          Hint: http://www.openvdb.org/documentation/doxygen/codeExamples.html **************/



unsigned int start_radius;
unsigned int end_radius;
unsigned int bound_x;
unsigned int bound_y;

unsigned int endx;
unsigned int endy;

unsigned int start_x;
unsigned int start_y;


bool**
MapLoader::loadVDBMap(char* filename)
{
    openvdb::initialize();
	// Create a VDB file object.
	openvdb::io::File file("maps/map1.vdb");
	// Open the file.  This reads the file header, but not any grids.
	file.open();
	// Loop over all grids in the file and retrieve a shared pointer
	// to the one named "LevelSetSphere".  (This can also be done
	// more simply by calling file.readGrid("LevelSetSphere").)
	openvdb::GridBase::Ptr baseGrid;
	for (openvdb::io::File::NameIterator nameIter = file.beginName();
		nameIter != file.endName(); ++nameIter)
	{
		// Read in only the grid we are interested in.
		if (nameIter.gridName() == "SDF") 
		{
			baseGrid = file.readGrid(nameIter.gridName());
		} 
	}
	file.close();

	openvdb::FloatGrid::Ptr grid = openvdb::gridPtrCast<openvdb::FloatGrid>(baseGrid);

    std::string valueAsString;
	openvdb::Metadata::Ptr value;
	int valueAsint;
	for (openvdb::MetaMap::MetaIterator iter = grid->beginMeta();
    iter != grid->endMeta(); ++iter)
	{
        const std::string& name = iter->first;
	value = iter->second;
	valueAsString = value->str();;
	if(name=="boundx"){
		value = iter->second;
		valueAsString = value->str();	
		bound_x=std::stoi(valueAsString);
		}
	else if(name=="startx"){
		value = iter->second;
		valueAsString = value->str();	
		start_x=std::stoi(valueAsString);
	}
	else if(name=="starty"){
		value = iter->second;
		valueAsString = value->str();	
		start_y=std::stoi(valueAsString);
	}
	else if(name=="endx"){
		value = iter->second;
		valueAsString = value->str();	
		endx=std::stoi(valueAsString);
	}
	else if(name=="endy"){
		value = iter->second;
		valueAsString = value->str();	
		endy=std::stoi(valueAsString);
	}
	else if(name=="boundy"){
		value = iter->second;
		valueAsString = value->str();	
		bound_y=std::stoi(valueAsString);
		}
	else if(name=="start_radius"){
		value = iter->second;
		valueAsString = value->str();	
		start_radius=std::stoi(valueAsString);
		}
	else if(name=="end_radius"){
		value = iter->second;
		valueAsString = value->str();	
		end_radius=std::stoi(valueAsString);
		}
	}	

    // Create the map data - 2D map and SDF array. Allocate memory
	

    //Get an accessor for coordinate-based access to voxels.
    
    /* Iterate through SDF array using the accessor to get SDF values. 
       Add true or false to map data according to the SDF value. */


    //Get values of Vectors posStart and posEnd

    //Change "loaded" flag

    //Return map data array
}

float**
MapLoader::getSDF(){


}

MapLoader::MapLoader() {

}

Vec2f
MapLoader::getStartPosition() {

}

Vec2f
MapLoader::getEndPosition() {

}

unsigned int
MapLoader::gety_boundary() {
	return 0;
}

unsigned int
MapLoader::getx_boundary() {
	return 0;
}

unsigned int
MapLoader::getStartRadius() {
	return start_radius;
}

unsigned int
MapLoader::getEndRadius() {
	return end_radius;
}
